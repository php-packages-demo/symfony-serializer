# symfony/serializer

https://packagist.org/packages/symfony/serializer

[![PHPPackages Rank](http://phppackages.org/p/symfony/serializer/badge/rank.svg)](http://phppackages.org/p/symfony/serializer)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/serializer/badge/referenced-by.svg)](http://phppackages.org/p/symfony/serializer)

## Unofficial documentation
* [*Another way for a Symfony API to ingest POST requests - No Bundle*
  ](https://dev.to/gmorel/another-way-for-a-symfony-api-to-ingest-post-requests-no-bundle-5h15)
  2020-07 Guillaume MOREL
* [*The Problem with JsonSerializable and Doctrine when using Symfony*
  ](https://theiconic.tech/the-problem-with-jsonserializable-and-doctrine-when-using-symfony-ad760e986b04?gi=1300330197a4)
  2020-06 Nootan Ghimire
* [*Using Symfony Serializer to consume REST APIs in OOP way*
  ](https://medium.com/infostud/using-symfony-serializer-to-consume-rest-apis-in-oop-way-9c5de319ef7b)
  2019-11 Nebojša Kamber